jQuery( function ( $ ) {
    const $document = $( this ),
        $table = $( '#domains_table' ).DataTable( {
            paging : false
        } );

    $document.on( 'submit', '#search-form', function () {
        const $this = $( this ),
            $input = $this.find( 'input[type="text"]' );

        $table.search( $input.val() ).draw();
    } );
} );